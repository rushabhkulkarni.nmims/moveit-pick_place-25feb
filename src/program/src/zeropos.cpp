#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

//////////
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/Point.h"

#include <sstream>
#define FRAME_LEN 8

int main(int argc, char **argv)
{
	/////////////////////////////////
	int i, j = 0;
	int s;
	int ret;
	int ret1, ang;
	// int flag=0;

	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;
	struct can_frame frame2;
	float RPS;
	int combinedMTRST3;

	// printf("CAN Sockets Demo\r\n");

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
	{
		perror("Socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "can0");
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("Bind");
		return 1;
	}

	

	/////////////////////////////////////////////////////////////////
	// Initiate new ROS node named "talker"
	ros::init(argc, argv, "zeropos_node");

	// create a node handle: it is reference assigned to a new node
	ros::NodeHandle n;
	// create a publisher with a topic "chatter" that will send a String message
	ros::Publisher chatter_publisher = n.advertise<std_msgs::Int32>("zeropos_Data", 10);
	// Rate is a class the is used to define frequency for a loop. Here we send a message each two seconds.
	ros::Rate loop_rate(100); // 1 message per second

	int count = 0;
	
		std_msgs::Int32 msg;
		

		for(int z=0;z<3;z++){
			
			int id=321+z;


		/////////////////////////////////////////////////////////////////////////////////////////
		struct can_frame frame_MTRStatus2_tx;
		struct can_frame frame_MTRStatus2_rx;
		int speeddata;

		frame_MTRStatus2_tx.can_id = id;
		frame_MTRStatus2_tx.can_dlc = FRAME_LEN;
		frame_MTRStatus2_tx.data[0] = 0xA4;
		frame_MTRStatus2_tx.data[1] = 0x00;
		frame_MTRStatus2_tx.data[2] = 0xD2;
		frame_MTRStatus2_tx.data[3] = 0x00;
		frame_MTRStatus2_tx.data[4] = 0x00;
		frame_MTRStatus2_tx.data[5] = 0x00;
		frame_MTRStatus2_tx.data[6] = 0x00;
		frame_MTRStatus2_tx.data[7] = 0x00;

		if (write(s, &frame_MTRStatus2_tx, sizeof(struct can_frame)) != sizeof(struct can_frame))
		{
			perror("Write");
		}
		msg.data=id;
		
		}
		
		
	
		

		//}
		///////////////////////////////////////////////////////////////////////////////////////////////////

		// create a string for the data
		std::stringstream ss;
		ss << "Hello World " << count;
		// assign the string data to ROS message data field
		// msg.data = ss.str();

		// print the content of the message in the terminal
		ROS_INFO("temp %d\n", msg.data);

		// Publish the message
		chatter_publisher.publish(msg);

		ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages

		loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
		count++;
	
	return 0;
}
