/*
 * Author: Anis Koubaa for Gaitech EDU
 * Year: 2016
 *
 */

#include "ros/ros.h"

#include <program/armcmd.h>
#include <sensor_msgs/JointState.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// #include "in.hpp"
//#include <conio.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

ros::Publisher telem_pub;
#define DEG_TO_RAD 0.01745329251
#define RAD_TO_DEG 57.2957795131
#define FRAME_LEN 8
#define POS_CLOSEDLOOP_1 0XA4

int s, p;
long store1 = 0;
unsigned long store2 = 0;

std::vector<double> old(8,0);
std::vector<double> control;
std::vector<double> compare;

// Topic messages callback
void chatterCallback(const sensor_msgs::JointState::ConstPtr &msg)
{

	bool change = false;
	old.resize(msg->position.size());
	control.resize(msg->position.size());
	compare.resize(msg->position.size());
	// printf("logic\n");

	// program::armcmd telem;

	for (int i = 0; i < 3; i++)
	{

		if (old[i] != msg->position[i])

		{

			change = true;
			//printf("flag true\n");
			i = 6;
		}
	}

	if (change)
	{
		//printf("flag true\n");
		for (int i = 0; i < 3; i++) //; i++)
		{
			printf("joint %d \n", i);
			printf("positions before %f \n", msg->position[i] * RAD_TO_DEG);

			
			control[i] = msg->position[i] * RAD_TO_DEG;

			control[0]=control[0]* (-1);
			control[1]=control[1]* (-1);
			control[2]=control[2]* (-1);
			//  int two2;
		    //  int three2;

			// if((control[i]-old[i]<18) && (control[i]-old[i]>-18))
			// {
			

			// compare[i]=(control[i]-old[i])*6;   
			// printf("angle less than  18  %d",compare[i]);                     //for speed


			//  two2 = (compare[i] & 0x00FF) >> 0;
			//  three2 = (compare[i]  & 0xFF00) >> 8;
			// }
			// else
			// {


			// 	two2=0x6C;
			// 	three2=0x00;
			// 	printf("angle greater than 18  %d",compare[i]);                     //for speed

			// }




			
		

			struct can_frame frame_Pos1_tx;

			// int angle1;
			int id = 321 + i;
			printf(" \n id %d\n", id);

			
			if (control[i] >= 0)
			{

				store2 = 600 * control[i];
				printf("store2 positive  %ld \n\n", store2);
			}

			else
			{
				//printf("control data                   %d \n", control[i] * RAD_TO_DEG);
				store2 = 4294967294 + (600 * control[i]);

				printf("store2 negative  %ld \n\n", store2);
			}










			const int four1 = (store2 & 0x000000FF) >> 0;
			const int five1 = (store2 & 0x0000FF00) >> 8;
			const int six1 = (store2 & 0x00FF0000) >> 16;
			const int seven1 = (store2 & 0xFF000000) >> 24;
			frame_Pos1_tx.can_id = id;
			frame_Pos1_tx.can_dlc = FRAME_LEN;
			frame_Pos1_tx.data[0] = POS_CLOSEDLOOP_1;
			frame_Pos1_tx.data[1] = 0x00;
			frame_Pos1_tx.data[2] = 0XB4;
			frame_Pos1_tx.data[3] = 0X00;
			frame_Pos1_tx.data[4] = four1;
			frame_Pos1_tx.data[5] = five1;
			frame_Pos1_tx.data[6] = six1;
			frame_Pos1_tx.data[7] = seven1;

			if (write(s, &frame_Pos1_tx, sizeof(struct can_frame)) != sizeof(struct can_frame))
			{
				perror("Write");
			}

			// printf(" \n                    angle is                             %d \n",store1);

			old[i] = control[i];
		}
		//printf("\n");
	}

	///
}

int main(int argc, char **argv)
{
//////
	struct sockaddr_can addr;
	struct ifreq ifr;

	printf("CAN Sockets Demo\r\n");

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
	{
		perror("Socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "can0");
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("Bind");
		return 1;
	}
	printf("binding done\n");
////////
	
	ros::init(argc, argv, "listener_node");
	
	ros::NodeHandle node;

	
	ros::Subscriber sub = node.subscribe("joint_states", 100, chatterCallback);

	
	ros::spin();

	return 0;
}